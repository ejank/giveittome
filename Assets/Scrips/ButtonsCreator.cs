﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonsCreator : MonoBehaviour {
    public GameObject buttonPrefab;
	// Use this for initialization
	void Start () {
	    
	}

   public void createButtons()
    {
        foreach (Wish w in GameControl.instance.pool.wishes)
        {
            GameObject but = (GameObject)Instantiate(buttonPrefab, Vector3.zero, Quaternion.identity);
            but.transform.SetParent(transform);
            but.transform.localScale = new Vector3(1, 1, 1);
            but.transform.localPosition = new Vector3(0, 0, 0);
            but.GetComponentInChildren<Text>().text = w.wishName;
            //kazdy wish ma komponent wihsPicker, ktory ma metode wywolywana po onclick - pick
            //a takze zmienna wishOfThisButton
            but.GetComponent<wihsPicker>().wishOfThisButton = w;
        }
    }
    bool created = false;
	// Update is called once per frame
	void Update ()
    { 
        
	    if(!created&& GameControl.instance!=null&&GameControl.instance.ready)
        {
            createButtons();
            created = true;
            
        }
	}
}
