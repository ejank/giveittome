﻿using UnityEngine;
using System.Collections;
/*based on:
*/
[RequireComponent(typeof(BoxCollider2D))]

public class RotateSprite : MonoBehaviour
{

    private Camera myCam;
    private Vector3 screenPos;
    private float angleOffset;

    void Start()
    {
        myCam = Camera.main;
    }

    void Update()
    {
        //This fires only on the frame the button is clicked
        //if (Input.GetMouseButtonDown(0))
        //This fires while the button is pressed down
        //if (Input.GetMouseButton(0))->content on mouse drag
    }
    void OnMouseDown()
    {   //returns screen position of the object that the script is attached to and stores it in screenPos
        screenPos = myCam.WorldToScreenPoint(transform.position);

        /*Debug.Log("screenPos.x " + screenPos.x +" screenPos.y: " + screenPos.y);
          Debug.Log("Input.mousePosition.x " + Input.mousePosition.x + " Input.mousePosition.y: " + Input.mousePosition.y);
        */
        //gets vector between screen Input.mousePosition and screen item position
        Vector3 v3 = Input.mousePosition - screenPos;
        //calculate difference between sprite current starting angle and 0Deg
        angleOffset = (Mathf.Atan2(transform.right.y, transform.right.x) - Mathf.Atan2(v3.y, v3.x)) * Mathf.Rad2Deg;
    }


    void OnMouseDrag()
    {   //gets vector between screen Input.mousePosition and screen item position
        Vector3 v3 = Input.mousePosition - screenPos;
        float angle = Mathf.Atan2(v3.y, v3.x) * Mathf.Rad2Deg;
        //set  sprite to mouse angle + difference between sprite "starting" angle and 0/
        transform.eulerAngles = new Vector3(0, 0, angle + angleOffset);
    }
}