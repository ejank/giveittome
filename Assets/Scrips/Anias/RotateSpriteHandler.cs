﻿using UnityEngine;
using System.Collections;

/*based on: Rotate Sprite
Parent - the sprite that will be rotated by handler required BoxColider2d*/
//[RequireComponent(typeof(BoxCollider2D))]

public class RotateSpriteHandler : MonoBehaviour
{

    private Camera myCam;
    private Vector3 screenPos;
    private float angleOffset;
    private GameObject my_parent;
    private Transform my_parent_transform;
    void Start()
    {
        if (Player.instance.isGhost())
        {
            Destroy(gameObject);
        }
        
        
            myCam = Camera.main;
            my_parent = gameObject.transform.parent.gameObject;
            my_parent_transform = gameObject.transform.parent;
        
            /*
        if (!my_parent.GetComponent<BoxCollider2D>())
        {
            my_parent.AddComponent<BoxCollider2D>();// Add the component
            Debug.LogError("Parent did not have BoxCollider2D. It was added automatically", transform.parent); // Throw an error
        }
        */
    }

    void Update()
    {
       // RectTransform.parent.gameObject.AddComponent<BoxCollider2D>();
    }
    void OnMouseDown()//mouse is down on handdler
    {   //returns screen position of the parent  of the obect the script is attached to and stores it in screenPos

        if (Player.instance.isGhost())
            return;
        screenPos = myCam.WorldToScreenPoint(my_parent_transform.position);

        /*Debug.Log("screenPos.x " + screenPos.x +" screenPos.y: " + screenPos.y);
          Debug.Log("Input.mousePosition.x " + Input.mousePosition.x + " Input.mousePosition.y: " + Input.mousePosition.y);
        */
        //gets vector between screen Input.mousePosition and screen item position
        Vector3 v3 = Input.mousePosition - screenPos;
        //calculate difference between sprite current starting angle and 0Deg
        angleOffset = (Mathf.Atan2(my_parent_transform.right.y, my_parent_transform.right.x) - Mathf.Atan2(v3.y, v3.x)) * Mathf.Rad2Deg;
    }


    void OnMouseDrag()
    {   //gets vector between screen Input.mousePosition and screen item position
        Vector3 v3 = Input.mousePosition - screenPos;
        float angle = Mathf.Atan2(v3.y, v3.x) * Mathf.Rad2Deg;
        //set  sprite to mouse angle + difference between sprite "starting" angle and 0/
        my_parent_transform.eulerAngles = new Vector3(0, 0, angle + angleOffset);
    }
}