﻿using UnityEngine;
using System.Collections;

/*based on :http://answers.unity3d.com/questions/12322/drag-gameobject-with-mouse.html
read also: touch - procesor directives: http://answers.unity3d.com/questions/562320/dragging-game-objects-with-mouse-touch-mobile.html
*/
[RequireComponent(typeof(BoxCollider2D))]

public class DragSprite : MonoBehaviour
{
    private Camera myCam;
    private Vector3 screenPos;
    private Vector3 offset;
    
    void Start()
    {
        myCam = Camera.main;
    }
    void OnMouseDown()
    {
        if (Player.instance.isGhost())
            return;
        screenPos = myCam.WorldToScreenPoint(gameObject.transform.position);

        offset = gameObject.transform.position - myCam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPos.z));

    }

    void OnMouseDrag()
    {
        if (Player.instance.isGhost())
            return;
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPos.z);

        Vector3 curPosition = myCam.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;

    }
    /*
    void OnMouseDrag()
    {
        float distance_to_screen = myCam.WorldToScreenPoint(gameObject.transform.position).z;
        Vector3 pos_move = myCam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance_to_screen));
        transform.position = new Vector3(pos_move.x, transform.position.y, pos_move.z);

    }
    */

}
