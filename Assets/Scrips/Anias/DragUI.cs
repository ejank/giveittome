﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

public class DragUI : NetworkBehaviour, IPointerDownHandler, IPointerUpHandler
{

    private bool mouseDown = false;
    private Vector3 startMousePos;
    private Vector3 startPos;


    public void OnPointerDown(PointerEventData ped)
    {
        Debug.Log("pointer down");
        mouseDown = true;
        transform.SetParent(ObjectHandlers.instance.commonCanvas.transform);
        startPos = transform.position;
        startMousePos = Input.mousePosition;
        
    }

    public void OnPointerUp(PointerEventData ped)
    {
        Debug.Log("pointer up");
        mouseDown = false;
        GameObject obj = (GameObject)Instantiate(GetComponent<itemButton>().itemOfThisButton, transform.position, Quaternion.identity);
        NetworkServer.Spawn(obj);
        Destroy(gameObject);
    }


    void Update()
    {
        if (mouseDown)
        {
            Vector3 v= Camera.main.ScreenToWorldPoint(Input.mousePosition);
            v.z = 90;
            transform.position = v;
            Debug.Log("poz1 " + transform.position);
            Vector3 currentPos =  Input.mousePosition;
            Vector3 diff = currentPos - startMousePos;
            Vector3 pos = startPos + diff;
            //transform.position = pos;
            Debug.Log("poz2 "+transform.position);
        }
    }
}