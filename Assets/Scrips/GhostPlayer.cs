﻿using UnityEngine;
using System.Collections;
using System;

public class GhostPlayer : PlayerControl
{
    public override void createAndStart()
    {
        ObjectHandlers.instance.ghostCanvas.SetActive(true);
    }
}
