﻿using UnityEngine;
using System.Collections;

abstract public class PlayerControl {

    public abstract void createAndStart();
    public virtual void pickWish(Wish w)
    {
        w.spwanEffect();
        if(w.wishName==GameControl.instance.pool.chosen.wishName)
        {
            ObjectHandlers.instance.commonCanvas.GetComponent<commonUI>().show(commonUI.whatToShow.WIN);
        }
        else
        {
            ObjectHandlers.instance.commonCanvas.GetComponent<commonUI>().show(commonUI.whatToShow.WRONG);
        }
    }
}
