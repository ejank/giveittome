﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class WhishCollection  {
    public Wish chosen;
    public List<Wish> wishes = new List<Wish>();
	

    internal static WhishCollection genereatePool()
    {
        WhishCollection ret = new WhishCollection();
        ret.wishes.Add(new Wish("Health for mother-in-law"));
        ret.wishes.Add(new Wish("Happiness"));
        ret.wishes.Add(new Wish("Holiday in the mountains"));
        ret.wishes.Add(new Wish("Mustang"));
        ret.wishes.Add(new Wish("Win a Lottery"));
        ret.wishes.Add(new Wish("Get breasts augmentation"));
        ret.wishes.Add(new Wish("Visit the Museum of the Countryside is SIERPC!"));
        ret.wishes.Add(new Wish("Pass  an exam"));
        ret.wishes.Add(new Wish("Get American Visa"));
        ret.wishes.Add(new Wish("Become a Game Developer"));
        ret.wishes.Add(new Wish("Pay off the mortgage"));
        ret.wishes.Add(new Wish("Fix bugs in my code"));
        ret.chosen = ret.wishes[(int)Random.Range(0, ret.wishes.Count)];
        return ret;

    }
}

