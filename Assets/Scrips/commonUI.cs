﻿using UnityEngine;
using System.Collections;

public class commonUI : MonoBehaviour {
    public GameObject win, lose, wrong;
    
    public Animator ghost;
    public enum whatToShow { WIN,LOSE,WRONG}
    public void show(whatToShow s)
    {
        
        switch(s)
        {
            case whatToShow.WIN:
                ghost.SetBool("happy", true);
                win.SetActive(true);
            break;
            case whatToShow.LOSE:
                ghost.SetBool("sad", true);
                lose.SetActive(true);
            break;
            case whatToShow.WRONG:
                ghost.SetBool("sad", true);
                wrong.SetActive(true);
            break;
        }
        Invoke("hide", 2);
    }
    void hide()
    {
        win.SetActive(false);
        lose.SetActive(false);
        wrong.SetActive(false);
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
