﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Item : NetworkBehaviour {
    [SyncVar]
    public string spriteName;
    public GameObject sprite;
	// Use this for initialization
	void Start () {
	    if(!isServer)
        {
            Debug.Log("creatin sprite name " + spriteName);
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("items/"+spriteName);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
