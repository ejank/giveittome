﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;

public class Player :NetworkBehaviour {
    public static Player instance;
    PlayerControl activePlayer;
    public bool isGhost()
    {
        if (activePlayer is GhostPlayer)
            return true;
        else
            return false;
    }
    public  virtual void pickWish(Wish wishOfThisButton)
    {
        activePlayer.pickWish(wishOfThisButton);
        if(!isServer)
        {
            CmdPickWish(JsonUtility.ToJson(wishOfThisButton));
        }
    }
    [Command]
    void CmdPickWish(string wishJson)
    {
        activePlayer.pickWish(JsonUtility.FromJson<Wish>(wishJson));
    }

    // Use this for initialization
    void Start () {
        if (isLocalPlayer)
            instance = this;
	    if(isServer)
        {
            activePlayer = new HumanPlayer();
            GameControl.instance.pool = WhishCollection.genereatePool();
            RpcSyncWishes(JsonUtility.ToJson(GameControl.instance.pool));
            GameControl.instance.ready = true;
        }
        else
        {
            activePlayer = new GhostPlayer();
        }
        activePlayer.createAndStart();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    [ClientRpc]
    void RpcSyncWishes(string pooljson)
    {

        Debug.Log(pooljson);
        GameControl.instance.pool = JsonUtility.FromJson<WhishCollection>(pooljson);
        GameControl.instance.ready = true;
    }
}
