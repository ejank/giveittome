﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using UnityEngine.UI;

public class ItemCollection : NetworkBehaviour {
    public GameObject baseItemPrefab;
    public GameObject buttonPrefab;
    public List<GameObject> items;
	// Use this for initialization
	void Start () {
        Sprite[]sprites= Resources.LoadAll<Sprite>("items");

        Debug.Log("sprites length " + sprites.Length);
        foreach(Sprite t in sprites)
        {
            GameObject obj = (GameObject)Instantiate(baseItemPrefab, new Vector3(-100, -100, -100), Quaternion.identity);
            obj.GetComponent<SpriteRenderer>().sprite = t;
            obj.GetComponent<Item>().spriteName = t.name;
            Debug.Log("spritenam " + t.name);
            items.Add(obj);
        }
	    if(isServer)
        {
            StartCoroutine(spawnItem());
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator spawnItem()
    {
        while(true)
        {
           GameObject i= items[Random.Range(0, items.Count)];

            GameObject but = (GameObject)Instantiate(buttonPrefab, Vector3.zero, Quaternion.identity);
            but.transform.SetParent(ObjectHandlers.instance.humanCanvas.GetComponent<HumanUI>().buttonsPlace.transform);
            but.transform.localPosition = Vector3.zero;
            but.transform.localScale = new Vector3(1,1,1);
            but.GetComponent<itemButton>().spritePlace.sprite= i.GetComponent<SpriteRenderer>().sprite;            
            but.GetComponent<itemButton>().itemOfThisButton = i;
            if (first < 0)
                yield return new WaitForSeconds(10f);
            else
                first--;
        }
    }
    int first = 3;
}
