﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class NetworkUI : MonoBehaviour {
    public Text address;
    public NetworkManager manager;
    public GameObject panel;
    public GameObject waitFor;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void host()
    {
        manager.StartHost();
        panel.SetActive(false);
        waitFor.SetActive(true);
    }

    public void join()
    {
        manager.StartClient();
        panel.SetActive(false);
        waitFor.SetActive(true);
    }

    public void change(string s)
    {
        manager.networkAddress = s;
    }
}
